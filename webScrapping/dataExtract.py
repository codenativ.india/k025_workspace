import requests
from bs4 import BeautifulSoup

soup = BeautifulSoup('''
        <div>
            <span class="ratings">5</span>
            <span class="views">100</span>
        </div>
        <div>
            <span class="ratings">10</span>
            <span class="views">92</span>
        </div>
''','html.parser')
views = soup.find_all("span",class_="views").contents[0]
print(views)