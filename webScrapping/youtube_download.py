# use "home" environment
import youtube_dl

link = ['https://www.youtube.com/watch?v=Veomn0tMs74']
print()
ydl_options = {
    'format':'worst',
    'writethumbnail':'writethumbnail',
    'writesubtitles':'writesubtitles'
}

with youtube_dl.YoutubeDL(ydl_options) as ydl:
    r = ydl.extract_info(link[0],download=False)
print(r['url'])

#############################################################cls
