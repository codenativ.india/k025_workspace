import requests
from bs4 import BeautifulSoup

soup = BeautifulSoup(requests.get("https://www.imdb.com/chart/top/").content, "html.parser")
top_ten_list = []

distro_tds = soup("td",class_="titleColumn", limit=100)

for element in distro_tds:
    top_ten_list.append(element.find("a").contents[0])

for i,n in enumerate(top_ten_list,1):
    print(f"{i}. {n}")
