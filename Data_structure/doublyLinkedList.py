class Node:
    def __init__(self,val=None):
        self.val = val
        self.next = None
        self.prev = None

class doublyLinkedList:
    def __init__(self):
        self.head = None

    def addingElements(self,data):
        data = Node(data)
        data.next = self.head
        if self.head is not None:
            self.head.prev = data
        self.head = data

    def displayElements(self,node):
        while (node is not None):
            print(node.val,end=" "),
            node = node.next

ddlink = doublyLinkedList()
ddlink.addingElements(45)
ddlink.addingElements(35)
ddlink.addingElements(25)
ddlink.addingElements(15)
(ddlink.displayElements(ddlink.head))
