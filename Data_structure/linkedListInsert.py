class Node:
    def __init__(self,datavalue=None):
        self.datavalue = datavalue
        self.nextvalue = None

class LinkedList:
    def __init__(self):
        self.headvalue = None
    
    def printList(self):
        printvalue = self.headvalue

        while printvalue:
            print(printvalue.datavalue)
            printvalue = printvalue.nextvalue

    def AtBegining(self,data):
        Newdata = Node(data)
        Newdata.nextvalue = self.headvalue
        self.headvalue = Newdata

link = LinkedList()
link.headvalue = Node("Monday")
element2 = Node("Tuesday")
element3 = Node("Wednesday")

## assigning elements
link.headvalue.nextvalue = element2
element2.nextvalue = element3

link.AtBegining("sunday")
link.printList()

