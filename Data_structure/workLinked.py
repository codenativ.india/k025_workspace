class Node:
    def __init__(self,val=0, next=None):
        self.val = val 
        self.next = next

class Solution:
    def get_decimal_point(self, head:Node):
        curr = head
        lst = []
        while curr:
            lst.append(curr.val)
            curr = curr.next
        print(lst)


link = Solution()
link.get_decimal_point([1,0,0,1,0,0,1,1,1,0,0,0,0,0,0])