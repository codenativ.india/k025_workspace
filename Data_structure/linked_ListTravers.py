class Node:
    def __init__(self,datavalue=None):
        self.datavalue = datavalue
        self.nextvalue = None

class SLinked:
    def __init__(self):
        self.headvalue = None
    def printlist(self):
        printvalue = self.headvalue
        while printvalue is not None:
            print(printvalue.datavalue)
            printvalue = printvalue.nextvalue
            
link = SLinked()
link.headvalue = Node("Sunday")
ele2 = Node("Monday")
ele3 = Node("Tuesday")

link.headvalue.nextvalue = ele2
ele2.nextvalue = ele3

link.printlist()