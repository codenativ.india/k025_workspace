class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"Node {self.value}"


class BinarySearch():
    def __init__(self, head):
        self.head = head

    def add(self, new_node):
        current_node = self.head
        while current_node:
            if current_node.value == new_node.value:
                raise ValueError("Exists")
            if new_node.value < current_node.value:
                if current_node.left:
                    current_node = current_node.left
                else:
                    current_node.left = new_node
                    break
            elif new_node.value > current_node.value:
                if current_node.right:
                    current_node = current_node.right
                else:
                    current_node.right = new_node
                    break


tree = BinarySearch(Node(9))
tree.add(Node(5))
tree.add(Node(13))
tree.add(Node(18))
tree.add(Node(56))

j = tree.head.right
d = j.right
print(tree.head)
print(d.right)
print(tree.head.right)
