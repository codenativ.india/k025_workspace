# pylint: disable=unused-variable
# pylint: enable=too-many-lines
import string
class Node:
    def __init__(self,datavalue=None):
        self.datavalue = datavalue
        self.nextvalue = None
    
class linkedList:
    def __init__(self):
        self.headvalue = None
    
    def printList(self):
        thisvalue = self.headvalue
        while thisvalue is not None:
            print(thisvalue.datavalue)
            thisvalue = thisvalue.nextvalue
    
    def endInsert(self,data):
        newdata = Node(data)
        if self.headvalue is None:
            self.headvalue = newdata
            return
        last = self.headvalue
        while last.nextvalue:
            last = last.nextvalue
        last.nextvalue = newdata

link = linkedList()
link.headvalue = Node("Monday")
e2 = Node("Tuesday")
e3 = Node("Wednesday")

link.headvalue.nextvalue = e2
e2.nextvalue = e3

link.endInsert("Thursday")
link.endInsert("Friday")
link.printList()
