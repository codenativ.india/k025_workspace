class Queue:
    def __init__(self):
        self.queue = list()
    
    def addToQ(self,data):
        if data not in self.queue:
            self.queue.insert(0,data)
            return True
        return False
    
    def removeQ(self):
        if len(self.queue) > 1:
            return self.queue.pop()
    
    def display(self):
        return len(self.queue)

q = Queue()
q.addToQ("Bring")
q.addToQ("the")
q.addToQ("Store")
print("Before")
print(q.display())
print("after")
print(f"{q.removeQ()} removed element")
print(q.display())