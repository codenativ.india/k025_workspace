class Stack:
    def __init__(self):
        self.values = []
    def add(self,data):
        if data not in self.values:
            self.values.append(data)
            return True
        else:
            return False

    def remove(self):
        if len(self.values)<=0:
            print("No Item to be removed")
        else:
            self.values.pop()
    
    def display(self):
        if len(self.values)<=0:
            return False
        else:
            return self.values

obj = Stack()
obj.add("New")
obj.add("base")
obj.add("Next")
# obj.remove()
print(obj.display())