class Node:
    def __init__(self,datavalue=None):
        self.datavalue = datavalue
        self.nextvalue = None

class LinkedList:
    def __init__(self):
        self.headvalue = None

    def append(self,data):
        newdata = Node(data)
        if self.headvalue is None:
            self.headvalue = newdata
            return
        current = self.headvalue
        while current.nextvalue:
            current = current.nextvalue
        current.nextvalue = newdata

    def display(self):
        current = self.headvalue
        while current is not None:
            print(current.datavalue)
            current = current.nextvalue

    def delete(self,index):
        current = self.headvalue
        
link = LinkedList()
link.append(1)
link.append(2)
link.display()