class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
        self.prev = None

class doublyLinkedList:
    def __init__(self):
        self.head = None

    def push(self, data):
        newNode = Node(data)
        newNode.next = self.head
        if self.head is not None:
            self.head.prev = newNode
        self.head = newNode

    def list_(self, node):
        while node is not None:
            last = node
            print(f"*********{last.value}")
            print(node.value)
            node = node.next


dlist = doublyLinkedList()
dlist.push(15)
dlist.push(46)
dlist.push(23)
dlist.push(38)
dlist.list_(dlist.head)