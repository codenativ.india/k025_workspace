class Stack:
    def __init__(self):
        self.stack = []
    def add(self,data):
        if data not in self.stack:
            self.stack.append(data)
            return True
        else:
            return False
    def peek(self):
        return self.stack

obj = Stack()
obj.add("name")
obj.add("is")
obj.add("google")
obj.add("name")
obj.add("names")
print(obj.peek())