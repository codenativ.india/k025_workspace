class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertBetween(self,middleNode,newData):
        if middleNode is None:
            print("No middle value")
            return
        
        element = Node(newData)
        element.next = middleNode.next
        middleNode.next = element

    def printList(self):
        current = self.head
        while current:
            print(current.data)
            current = current.next

link = LinkedList()
link.head = Node("Monday")
e2 = Node("Tuesday")
e3 = Node("Thursday")

link.head.next = e2
e2.next = e3

link.insertBetween(e2,"Wednesday")
link.printList()