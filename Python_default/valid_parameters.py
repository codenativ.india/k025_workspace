def validParameters(s):
    s = s.replace(" ",'')
    comp = ['{}','[]','()']
    if len(s) == 0:
        return True

    for i,j in zip(s,s[1:]):
        if i+j in comp:
            base = s.replace(i,'')
            base = base.replace(j,'')
            return validParameters(base)
        else:
            pass
    return False

print(validParameters("(]"))
print(validParameters("[[[]]]"))
print(validParameters("{{({})}}"))
print(validParameters("{ { (      ) } }"))

