def apple_orange(house_location, tree_location, no_of_fruits, apple_distance,
                orange_distance):
    apple_count = 0
    orange_count = 0
    start, end = (house_location[0]), (house_location[1])
    apple_tree_location, orange_tree_location = tree_location[0], tree_location[1]
    # apples,oranges = no_of_fruits[0],no_of_fruits[1]
    apple_calc_x = [x+apple_tree_location for x in apple_distance]
    orange_calc_x = [y+orange_tree_location for y in orange_distance]

    for i in apple_calc_x:
        if (start <= i <= end):
            apple_count += 1
    for j in  orange_calc_x:
        if (start <= j <= end):
            orange_count += 1
    return [apple_count,orange_count]



house_location = list(input("house Location: ").split(' '))
house_location = list(map(int,house_location))

tree_locations = list(input("tree locations: ").split(' '))
tree_locations = list(map(int,tree_locations))

no_of_fruits = list(input("apple and orange numbers: ").split(' '))
no_of_fruits = list(map(int,no_of_fruits))

apple_distance = list(input("apple distances: ").split(' '))
apple_distance = list(map(int,apple_distance))

orange_distance = list(input("orange distances: ").split(' '))
orange_distance = list(map(int,orange_distance))

print(apple_orange(house_location,
                   tree_locations,
                   no_of_fruits,
                   apple_distance,
                   orange_distance))