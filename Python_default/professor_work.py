user = input()
li = []
count = 0

for i in range(int(user)):
    user_inp = list(input().split(' '))
    int_user = list(map(int,user_inp))
    student_count = int_user[0]
    professor_count = int_user[1]
    students = list(input().split(' '))
    int_students = list(map(int,students))

    for j in int_students:
        if j <= 0:
            count += 1
    
    if count >= professor_count:
        li.append("NO")
    else:
        li.append("YES")
    count = 0
print('\n'.join(li))