import re

def last_(term):
    if len(term.group(0)) == 5:
        return '***' + term.group(0)[-2:]
    elif len(term.group(0)) == 10:
        return '*******' + term.group(0)[-3:]
    else: return term.group(0)

s = "User_Id: 45612 and mobile number: 7894651320"
find = re.sub('\d+', last_, s)
print(find)
