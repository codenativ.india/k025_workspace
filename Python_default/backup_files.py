def boxSize(symbol,height,width):
    if len(symbol) != 1:
        raise Exception("New Errors are arraised")
    if height <= 2:
        raise Exception("Height error occured")
    if width <= 2:
        raise Exception("Width Error Occured")

    # area = height*width
    # print(f"Area {area} Symbol : {symbol}")
    print(symbol*width)
    for i in range(height-2):
        print(symbol + (' '*(width-2))+symbol)
    print(symbol*width)

for symb, hei, wid in (('*',4,4),('&',2,9),('$',5, 20),):
    try:
        boxSize(symb,hei,wid)
    except Exception as error:
        print(f"An Exceptoin occured {error}")
    