def remove_duplicates(items): 
    unique_items = set()
    for item in items:
        if item not in unique_items:
            yield item 
            unique_items.add(item)

emails = ['rob@gmail.com', 'mike@gmail.com',  'mike@gmail.com', 
          'sara@gmail.com',  'mike@gmail.com', 'fred@gmail.com']

print(list(remove_duplicates(emails)))
# val = item if key is None else key(item) 
# print(list
# (remove_unhashable_duplicates(patients, key=lambda d: (d['name'],d['medical_charges'],d['medical_procedure']))))