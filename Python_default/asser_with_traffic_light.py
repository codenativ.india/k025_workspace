def trafficLight(stopLight):
    for key in stopLight.keys():
        if stopLight[key] == "green":
            stopLight[key] = "yellow"
        elif stopLight[key] == "yellow":
            stopLight[key] = "red"
        elif stopLight[key] == "red":
            stopLight[key] = "green"
    stopLight = stopLight
    print(stopLight)
    assert 'yellow' in stopLight.values(), "Neither Light is red" + str(stopLight)
(trafficLight({"ns":"green","ew":"red"}))