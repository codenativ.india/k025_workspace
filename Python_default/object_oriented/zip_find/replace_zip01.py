import os
import zipfile
import shutil


class ZipProcessor:
    def __init__(self, filename, processor):
        self.filename = filename
        self.processor = processor
        self.temp_folder = f"unzipped-{filename}"

    def full_filename(self, filename):
        return os.path.join(self.temp_folder, filename)

    def process_zip(self):
        self.unzip_files()
        self.processor.process(self)
        self.zip_files()

    def unzip_files(self):
        os.mkdir(self.temp_folder)
        main_zip = zipfile.ZipFile(self.filename)
        try:
            main_zip.extractall(self.temp_folder)
        finally:
            main_zip.close()

    def zip_files(self):
        final_zip = zipfile.ZipFile(f"@c-{self.filename}", 'w')
        for filename in os.listdir(self.temp_folder):
            final_zip.write(self.full_filename(filename), filename)
        shutil.rmtree(self.temp_folder)
