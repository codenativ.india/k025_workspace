from replace_zip01 import ZipProcessor
import os
import sys


class ZipReplace:
    def __init__(self, search_string, replace_string):
        self.search_string = search_string
        self.replace_string = replace_string

    def process(self, zip_processor):
        for filename in os.listdir(zip_processor.temp_folder):
            with open(zip_processor.full_filename(filename)) as file:
                text = file.read()
            text = text.replace(self.search_string, self.replace_string)
            with open(zip_processor.full_filename(filename), 'w') as file:
                file.write(text)


if __name__ == "__main__":
    se = input("search: ")
    re = input("replace: ")
    f_n = input("filename: ")
    zip_replace = ZipReplace(se, re)
    ZipProcessor(f_n, zip_replace).process_zip()
