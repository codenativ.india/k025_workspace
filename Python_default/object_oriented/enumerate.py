import sys

try:
    filename = sys.argv[1]
except IndexError:
    print("Enter a File to enumerate..")
    sys.exit()
with open(filename) as file:
    for index, line in enumerate(file):
        print("{0} {1}".format(index+1,line), end='')
    print('')
