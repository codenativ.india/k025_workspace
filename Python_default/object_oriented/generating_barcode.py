import barcode
from barcode.writer import ImageWriter
from io import BytesIO

class Customer(object):
    
    def __init__(self, name, barcode=None):
        self.name = name
        self.barcode = barcode

    def generate_barcode(self):
        COD128 = barcode.get_barcode_class('code128')
        rv = BytesIO()
        code = COD128(f'{self.name}',writer=ImageWriter()).write(rv)
        self.barcode = code
    