class LongestDict(dict):
    
    def longest_key(self):
        longest = None
        for key in self:
            if (not longest) or (len(key) > len(longest)):
                longest = key
        return longest


a = LongestDict()
a['f'] = 1
a['second'] = 2
a['hundered'] = 100
print(a.longest_key())