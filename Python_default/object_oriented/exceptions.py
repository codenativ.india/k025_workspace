class EvenOnly(list):
    def append(self,integer):
        if not isinstance(integer,int):
            raise TypeError("Only Integers")
        if integer % 2:
            raise ValueError("Only Event numbers")
        super().append(integer)