python3 -c "from text_editor import Document, Character;
a = Document();
a.insert('h');
a.insert('e');
a.insert('l');
a.insert('l');
a.insert('o');
a.insert('\n');

a.insert('w');
a.insert('o');
a.insert('r');
a.insert('l');
a.insert('d');
a.insert('!');
a.insert('\n');
a.insert('\n');


a.cursor.doc_home();
a.insert('p');
a.insert('y');
a.insert('t');
a.insert('h');
a.insert('o');
a.insert('n');
a.insert('\n');

a.cursor.doc_end();
a.insert('\n');
a.insert('c');
a.insert('l');
a.insert('a');
a.insert('n');
a.insert('g');
a.cursor.line_home();
a.insert('C');
a.insert('\n');
a.batch_edit(1, 5, 'italic');
a.batch_edit(6,11,'bold');
print(a.string);"
