class BaseClass:
    num_base_cells = 0
    def call_me(self):
        print("Inittializing the base class")
        self.num_base_cells += 1

class LeftBaseClass(BaseClass):
    num_left_cells = 0
    def call_me(self):
        super().call_me()
        print("Intializing the left base class")
        self.num_left_cells += 1

class RightBaseClass(BaseClass):
    num_right_cells = 0
    def call_me(self):
        super().call_me()
        print("Initializing the right base class")
        self.num_right_cells += 1

class BottomBaseClass(LeftBaseClass,RightBaseClass):
    num_bottom_cells = 0
    def call_me(self):
        super().call_me()
        print("Intializing the bottom base class")
        self.num_bottom_cells += 1
