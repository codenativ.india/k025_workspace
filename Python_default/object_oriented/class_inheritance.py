# class MyObject(object):
#     pass

class ContactList(list):
    def search(self, name):
        match_queries = []
        for match in self:
            if name in match.name:
                match_queries.append(match)
        return match_queries


class Contact:
    all_contacts = ContactList()
    def __init__(self, name, email):
        self.name = name
        self.email = email
        Contact.all_contacts.append(self)

class Supplier(Contact):
    def order(self,order):
        print(f"Order has been confirmed as {order} by {self.name}")


class Friend(Contact):
    def __init__(self,name,email,phone):
        super().__init__(name,email)
        self.phone = phone

class SendEmail:
    def send_email(self,message):
        print(f"sending email to the person name of {self.email}.")
        print(f'Message: {message}')

class EmailContact(Contact,SendEmail):
    pass