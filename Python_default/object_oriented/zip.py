import os 
import sys

filename = sys.argv[1]

contacts = []
with open(filename) as file:
    header = file.readline().strip().split('\t')
    for line in file:
        line = line.strip().split('\t')
        contact_map = zip(header,line)
        contacts.append(dict(contact_map))

for contact in contacts:
    print('{first} {last} --> Email: {email}'.format(**contact))


# def return_result(filename):
#     with open(filename) as file:
#         header = file.readline().strip().split(',')
#         contacts = [ dict(zip(header, line.strip().split(','))) for line in file]
#     return contacts