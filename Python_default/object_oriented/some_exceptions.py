# except BaseException

# Exception Main class
#   1. SystemExist
#   2. KeyboardInterrupt
#   3. Exceptions ---  { BaseException }
#                   1.TypeError
#                   2.ValueError
#                   3.IndexError
#                   etc....


import random
some_exp = [ValueError, TypeError, IndexError, None]
try:
    choice = random.choice(some_exp)
    print(f"Raising {choice}")
    if choice:
        raise choice("Funny Error")
except ValueError:
    print("Value is improper")
except TypeError:
    print("Type is improper")
except Exception as e:
    print(f"Various Errors {e.__class__.__name__}")
else:
    print("The code has no error")
finally:
    print("Program Completed")


