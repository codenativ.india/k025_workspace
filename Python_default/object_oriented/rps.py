import random
import sys

class Game:
    def __init__(self):
        self.game_choices = {'r':'rock','p':'paper','s':'scissor','q':'quit'}
        self.u_score = 0
        self.c_score = 0

    def start_game(self):
        game_input = input('Enter the choice: ').lower()
        try:
            u_choice = self.game_choices[game_input]
        except KeyError:
            print('THis Option is NoT avaiLable.')
            return self.start_game()
        if u_choice == 'quit':
            sys.exit(1)
        return self.game_points(u_choice, self.compute_play())
    
    def compute_play(self):
        c_choice = random.choice(list(self.game_choices.values()))
        return c_choice

    def game_points(self,user,computer):
        # user_points
        if (user == 'paper' and computer == 'rock') or (user == 'rock' and computer == 'scissor') \
             or (user == 'scissor' and computer == 'paper'):
            self.u_score += 1
            print(f'\n {"*"*10} You win {"*"*10} \n')
        elif (computer == 'paper' and user == 'rock') or (computer == 'rock' and user == 'scissor') \
                or (computer == 'scissor' and user == 'paper'):
            self.c_score += 1
            print(f'\n {"*"*10} Computer wins {"*"*10} \n')
        else:
            print(f'\n {"*"*10} Game Tied {"*"*10} \n')
        print(f"You score: {self.u_score},\n Computer: {self.c_score}")
        return self.start_game()
        
g = Game()
g.start_game()
