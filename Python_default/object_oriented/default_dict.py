# from collections import defaultdict
# num_items = 0

# def tuple_counter():
#     global num_items
#     num_items += 1
#     return (num_items,[])

# d = defaultdict(tuple_counter)

def calculate(sentence):
    d = dict()
    for i in sentence:
        d.setdefault(i, 0)
        d[i] = d[i]+1
    return d


print(calculate("abcdefghhgfedecba"))


# import string
# characters = list(string.ascii_letters) + [" "]

# def cal_list(sentence):
#     freq = [(c,0) for c in characters]
#     for letter in sentence:
#         character = characters.index(letter)
#         freq[character] = (letter,freq[character][1]+1)
#     return [i for i in freq if not i[1] == 0]

# print(cal_list("more efficient"))
