# supporting function is more applicable
def get_validation(question,choices):
    question += " ({})".format(', '.join(choices))
    response = input(question)
    while response.lower() not in choices:
        response = input(question)
    return response