def format_string(string, formatter=None):
    'Format the given string'

    class StringInstansiate:
        def format(self, string):
            return str(string).title()

    if not formatter:
        formatter = StringInstansiate()
    
    return formatter.format(string)

print(format_string('google is a search engine.','many'))