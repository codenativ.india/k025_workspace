# from relative_imports import Database

class Point:
    'Represent a point in two-dimensional'
    def __init__(self, x=0, y=0):
        '''Initialize the position if argument is
            not provided set the defafult values as 0''' 
        self.move(x,y)
    def move(self, x, y):
        'move the point to new location'
        self.x = x
        self.y = y
    def reset(self):
        'entierly reset the points'
        self.move(0,0)

c = Point(8,9)
print(c.x)
print(c.y)
c.move(10,15)
print(c.x)
print(c.y)
c.reset()
print(c.x)
print(c.7)
