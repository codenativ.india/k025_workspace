book_no, row = 0,1

class Book:
    def __init__(self,book_name):
        global book_no, row
        book_no += 1
        if book_no % 6 == 0:
            row += 1
        self.book_no = book_no
        self.row = row
        self.book = book_name
        self.borrow = False
    
    def return_details(self):
        print(f"""
        Book Name: {self.book},\n
        Book No: {self.book_no},\n
        Book row: {self.row},\n
        Status: {self.borrow}\n
        """)

class BookNotFound(Exception):
    pass

class BookInProgress(Exception):
    pass

class Library:
    def __init__(self):
        self.books = []

    def add_book(self,name):
        return self.books.append(Book(name))

    def list_all(self):
        for item in self.books:
            print(f"""
        Book Name: {item.book},\n
        Book No: {item.book_no},\n
        Book row: {item.row},\n
        Status: {item.borrow}\n
        """)

    def get_book(self,no):
        file_name = [book for book in self.books if str(book.book_no) == no]
        if not file_name:
            raise BookNotFound("No Book Found")
        if file_name:
            return file_name[0]
    
    def borrow_book(self,no):
        book = self.get_book(no) 
        if book.borrow == False:
            book.borrow = True
            print(f"You borrowed \"{book.book}\" book")

        else:
            print("sorry book is already borrwed")
    
    def delete_book(self,no):
        book = self.get_book(no)
        if book.borrow == True:
            raise BookInProgress("Borrowed by someone.")
        self.books.remove(book)
        print(f"Book of name {book.book} has been deleted successfully")