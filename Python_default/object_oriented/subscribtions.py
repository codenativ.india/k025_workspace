from datetime import datetime, timedelta

id = 0
plans = {'a':365,'m':30}

class User:
    def __init__(self,user,password):
        global id
        id += 1
        self.id = id
        self.user = user
        self.password = password
        self.plan = "Standard"
        self.subscription_date = None
        self.subscription_end_date = None

    def __repr__(self):
        return f"{self.user}"

    def subscribe(self,plan):
        try:
            days = plans[plan]
        except KeyError:
            print("Sorry the plan not exists")
            return
        if not self.subscription_date:
            self.plan = "Membership"
            self.subscription_date = (datetime.now()).date()
            self.subscription_end_date = (datetime.now()).date() + timedelta(days)
            self.details()
        else:
            print("New Features: we are extending your existing plan")
            current_end = self.subscription_end_date
            extending_date = current_end + timedelta(days)
            self.subscription_end_date = extending_date
            self.details()
            print("Thank you for staying connected...")


    def details(self):
        print(f"""
        ID:\t{self.id}
        Name:\t{self.user}
        Plan:\t{self.plan}
        Subscription:\t{self.subscription_date}
        Subscription EndDate:\t{self.subscription_end_date}\n
        """)

class WebUsers:
    def __init__(self):
        self.members = []

    def add_user(self,username,password):
        if username not in self.members:
            user = User(username,password)
            self.members.append(user)
            user.details()

    def view_user(self):
        for person in self.members:
            print(person)

    def find_user(self,member):
        for user in self.members:
            if member == user.user:
                return user
        print(f'Username of {member} doesn\'t exist in our database')
        return False

    def add_subscription(self,member,plan):
        user = self.find_user(member)
        if user:
            user.subscribe(plan)

ob = WebUsers()
ob.add_user('george', '123123')
ob.add_user('frank', 'iopiop')
ob.add_user('harry', 'tyutyu')
# ob.view_user()

ob.find_user('george')
ob.find_user('frank')
ob.find_user('harry')
ob.find_user('jared')

ob.add_subscription('george','a')
ob.add_subscription('frank','m')
ob.add_subscription('george','a')
