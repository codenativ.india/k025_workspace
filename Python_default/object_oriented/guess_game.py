import random
import os
import sys

class Guess:
    def __init__(self):
        self.number = random.randint(1,50)
        self.count = 1
        self.asked = list()
    
    def clues(self,option):
        all_clues = []
        # clue 1
        if option > self.number:
            all_clues.append("Your guess is too high!")
        else:
            all_clues.append("Your guess is too low!")
        # clue 2
        clue_2 = self.divisor(self.number)
        all_clues.append(f"""The number is divisble by {random.choice(list(clue_2))}""")
        # clue 3
        if self.number % 2 == 0:
            all_clues.append("It is an even number.")
        else:
            all_clues.append("It is an odd number.")
        if option in range(self.number-2,self.number+2):
            print("Nearly Close..Try Harder.")
        return all_clues
    
    # helping clue
    def divisor(self, number):
        divisors = list()
        i = 1
        while i <= number:
            if (number % i == 0):
                divisors.append(i)
            i += 1
        # divisors.append('You intution say the correct answer..')
        return [i for i in divisors if i != 1]

    def user(self,option):
        if self.count < 5:
            if option == self.number:
                if self.count == 1:
                    print("You are a genius, man!")
                else:
                    print(f"You got the answer in {self.count} attempts")
            else:
                self.count += 1
                choices_lst = self.clues(option)
                choice = random.choice(list(choices_lst))
                if choice not in self.asked:
                    self.asked.append(choice)
                    print(f"Hint: {choice}\n")
                else:
                    print('Try Your best\n')

                self.user(int(input(f"Enter the number: ")))
        
        else:
            print(f"The number is {self.number}")
            print("Better Luck Next time..")


g = Guess()
print("Guess the number between 1 - 50")
print("* You are allowed to make five Guesses.\n")
g.user(int(input("Enter the number: ")))
