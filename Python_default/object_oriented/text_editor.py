class Document:
    def __init__(self):
        self.doc_name = ''
        self.characters = []
        self.cursor = Cursor(self)
    
    def insert(self,character):
        if not hasattr(character,'character'):
            character = Character(character)
        self.characters.insert(self.cursor.position,character)
        self.cursor.forward()
    
    def batch_edit(self, start, end, format):
        styles = ['underline', 'bold', 'italic']
        if not format in styles:
            raise ValueError("Error Formatting style.")
        for i in range(start-1,end):
            setattr(self.characters[i],format,True)

    def delete(self):
        try:
            self.cursor.backward()
            del self.characters[self.cursor.position]
        except IndexError:
            print("No more characters in the documents.")
    
    def save(self):
        f = open(self.doc_name,'w')
        f.write(''.join(self.characters))
        f.close()

    @property
    def string(self):
        return "".join((str(c) for c in self.characters))

class Cursor:
    def __init__(self, document):
        self.document = document
        self.position = 0
    
    def forward(self):
        self.position += 1

    def backward(self):
        self.position -= 1
    
    def line_home(self):
        while self.document.characters[self.position -1].character != '\n':
            self.position -= 1
            if self.position == 0:
                break

    def line_end(self):
        while self.position < len(self.document.characters) and \
            self.document.characters[self.position].character != '\n':
            self.position += 1
    
    def doc_end(self):
        while self.position != len(self.document.characters):
            self.forward()
    
    def doc_home(self):
        while self.position != 0:
            self.backward()

    
class Character:
    def __init__(self,character,bold=False,italic=False,underline=False):
        assert len(character) == 1
        self.character = character
        self.italic = italic
        self.bold = bold
        self.underline = underline
    
    def __str__(self):
        bold = "*" if self.bold else ""
        italic = "/" if self.italic else ""
        underline = "_" if self.underline else ""
        return bold + italic + underline + self.character
    
# test calls 
# a = Document()
# a.insert('h')
# a.insert('e')
# a.insert('l')
# a.insert('l')
# a.insert('o')
# a.insert('\n')

# a.insert('w')
# a.insert('o')
# a.insert('r')
# a.insert('l')
# a.insert('d')
# a.insert('!')
# a.insert('\n')
# a.insert('\n')


# a.cursor.doc_home()
# a.insert('p')
# a.insert('y')
# a.insert('t')
# a.insert('h')
# a.insert('o')
# a.insert('n')
# a.insert('\n')

# a.cursor.doc_end()
# a.insert('\n')
# a.insert('c')
# a.insert('l')
# a.insert('a')
# a.insert('n')
# a.insert('g')
# a.cursor.line_home()
# a.insert('C')
# a.insert('\n')
# a.batch_edit(1, 5, 'italic')
# a.batch_edit(6,11,'bold')
# print(a.string)
