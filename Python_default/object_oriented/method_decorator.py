class Foo:
    @property
    def silly(self):
        print("Getting values")
        return self._silly
    
    @silly.setter
    def silly(self,value):
        print("making silly")
        self._silly = value
    
    @silly.deleter
    def silly(self):
        print("deleting silly")
        del self._silly

