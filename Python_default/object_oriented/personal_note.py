# essential features are to store the note in the seprate text file
# step 1: create a file based on the argv name
# sep 2: ask the use to enter the input through 
#        the text and open up the text editor
import sys
import os
import subprocess
from pathlib import Path

def add_notebook(name='base',text=''):
    file_ = f'{name}.txt'
    text = ' '.join(text)
    path = os.getcwd()
    new_path = os.path.join(path,'save_notes/',file_)
    print(new_path)
    if Path(new_path).exists():
        with open(f'{new_path}','a') as f:
            f.write(text)
            f.write('\n')
    else:
        with open(f'{new_path}', 'w') as f:
            f.write(text)
            f.write('\n')

def view_note(name):
    file_ = f'{name}.txt'
    path = os.getcwd()
    new_path = os.path.join(path, 'save_notes/', file_)
    if Path(new_path).exists():
        subprocess.Popen(['subl',new_path],shell=True)
    else:
        print('file is not found so create a new one.')


# def delete_task(line):
    
# try:
#     add_notebook(sys.argv[1],sys.argv[2])
# except IndexError:
#     add_notebook()
try:
    if sys.argv[1] == 'vn':
        view_note(sys.argv[2])
    elif sys.argv[1] == 'ad':
        add_notebook(sys.argv[2], sys.argv[3:])

except IndexError:
    print('Enter some arguments')