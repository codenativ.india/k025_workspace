# python3 -c "from default_dic import Book, Library
# a = Library();
# a.add_book('google');
# a.add_book('bing');
# a.add_book('hangout');
# a.list_all();
# a.borrow_book('1');
# a.borrow_book('1');
# a.delete_book('3');
# a.list_all();
# "
python3 -c "from object_methods import SortedDict;
ds = SortedDict();
d = {};
ds['a'] = 23;
ds['e'] = 27;
ds['c'] = 25;
ds['g'] = 30;
ds['i'] = 32;


d['a'] = 23;
d['e'] = 27;
d['c'] = 25;
d['g'] = 30;
d['i'] = 32;

for a,b in ds.items():
    print(a,b)

print('****************')

for c,d in d.items():
    print(c,d)
"