class Property:
    def __init__(self, square_feet='',num_bedrooms='',num_bathrooms='', available=True,**kwargs):
        super().__init__(*kwargs)
        self.square_feet = square_feet
        self.num_bedrooms = num_bedrooms
        self.num_bathrooms = num_bathrooms
        self.available = available

    def display(self):
        print("PROPERTY DETAILS")
        print("================")
        print("Square Feet: {}".format(self.square_feet))
        print("No of Bedrooms: {}".format(self.num_bedrooms))
        print("No of Bathrooms: {}".format(self.num_bathrooms))
        print("Property Status: {}".format(self.available))
        print()
    
    def purchased(self):
        self.available = False
        return True

    @staticmethod
    def prompt_init():
        return dict(
            square_feet=input("Enter the Square Feet: "),
            num_bedrooms=input("Enter the Bedroom nos: "),
            num_bathrooms=input("Enter the Bathroom nos: "),
            )

    # prompt_init = staticmethod(prompt_init)


def get_validation(question, choices):
    question += " ({}) ".format(', '.join(choices))
    response = input(question)
    while response.lower() not in choices:
        response = input(question)
    return response


class Apartment(Property):
    valid_laundries = ('coin','ensuite','none')
    valid_balcony = ('yes','no','solarium')

    def __init__(self,balcony='',laundry='',**kwargs):
        super().__init__(**kwargs)
        self.balcony = balcony
        self.laundry = laundry

    def display(self):
        super().display()
        print("APARTMENT DETAILS")
        print("Laundry: %s" % self.laundry)
        print("Balcony: %s" % self.balcony)

    @staticmethod
    def prompt_init():
        parent_init = Property.prompt_init()
        laundry = get_validation(
            "What Laundary facilities does the apartment have? ",
            Apartment.valid_laundries)
        balcony = get_validation(
            "What Laundary facilities does the apartment have? ",
            Apartment.valid_balcony)
        parent_init.update(
            {"laundry":laundry, "balcony":balcony}
        )
        return parent_init
    
    # prompt_init = staticmethod(prompt_init)


class House(Property):
    valid_garage = ('attached','detached','none')
    valid_fenced = ('yes','no')

    def __init__(self, fence='', garage='', num_stories='', **kwargs):
        super().__init__(**kwargs)
        self.fence = fence
        self.garage = garage
        self.num_stories = num_stories

    
    def display(self):
        super().display()
        print("HOUSE DETAILS")
        print("House of stories: {}".format(self.num_stories))
        print("Garage: {}".format(self.garage))
        print("Fence: {}".format(self.fence))

    @staticmethod
    def prompt_init():
        parent_init = Property.prompt_init()
        fenced = get_validation("Is fenced?",House.valid_fenced)
        garage = get_validation("Is there garage?",House.valid_garage)
        num_stories = input("Nos of Num stories: ")

        parent_init.update(
            {'fence':fenced, 'garage':garage, 'num_stories':num_stories}
        )
        return parent_init
    
    # prompt_init = staticmethod(prompt_init)


class Purchase:

    def __init__(self, price='', taxes='', **kwargs):
        super().__init__(**kwargs)
        self.price = price
        self.taxes = taxes

    def display(self):
        super().display()
        print("PURCHASE DETAILS")
        print("Selling Price: {}".format(self.price))
        print("Purchase Taxes: {}".format(self.taxes))

    @staticmethod
    def prompt_init():
        return dict(
            price=input("Enter selling Price: "),
            taxes=input("Enter Tax Price: ")
        )
    # prompt_init = staticmethod(prompt_init)
        

class Rental:
    
    def __init__(self, furnished='',utilities='',rent='',**kwargs):
        super().__init__(**kwargs)
        self.furnished = furnished
        self.utilities = utilities
        self.rent = rent
    
    def display(self):
        super().display()
        print("RENTAL DETAILS")
        print("Furnished: {}".format(self.furnished))
        print("Utilities: {}".format(self.utilities))
        print("Rent: {}".format(self.rent))

    @staticmethod
    def prompt_init():
        return dict(
            furnished=get_validation("Is furnished ? ",("yes","no")),
            utilities=input("Estimated utilities: "),
            rent=input("Rent Amount: ")
        )

    # prompt_init = staticmethod(prompt_init)


class HouseRental(Rental, House):
    @staticmethod
    def prompt_init():
        init = House.prompt_init()
        init.update(Rental.prompt_init())
        return init
    # prompt_init = staticmethod(prompt_init)


class HousePurchase(Purchase, House):
    @staticmethod
    def prompt_init():
        init = House.prompt_init()
        init.update(Purchase.prompt_init())
        return init
    # prompt_init = staticmethod(prompt_init)


class ApartmentRental(Rental, Apartment):
    @staticmethod
    def prompt_init():
        init = Apartment.prompt_init()
        init.update(Rental.prompt_init())
        return init
    # prompt_init = staticmethod(prompt_init)


class AppartmentPurchase(Purchase, Apartment):
    @staticmethod
    def prompt_init():
        init = Apartment.prompt_init()
        init.update(Purchase.prompt_init())
        return init
    # prompt_init = staticmethod(prompt_init)


class Agent:

    def __init__(self):
        self.property_list = []
    
    def display_properties(self):
        for property in self.property_list:
            property.display()

    type_maps = {
        ("house","rental"):HouseRental,
        ("house","purchase"):HousePurchase,
        ("apartment","rental"):ApartmentRental,
        ("apartment","purchase"):AppartmentPurchase
    }

    def add_property(self):
        property_type = get_validation("Enter the Property type: ",("house","apartment")).lower()
        payment_type = get_validation("Enter the Payment type: ",("rental","purchase")).lower()

        PropertyClass = self.type_maps[property_type,payment_type]
        prompt_args = PropertyClass.prompt_init()
        self.property_list.append(PropertyClass(**prompt_args))

    def return_available(self):
        count = 0
        for item in self.property_list:
            if item.available:
                count += 1
            return count