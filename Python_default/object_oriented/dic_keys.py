random_keys = {}
random_keys["astring"] = "some string"
random_keys[5] = "an integer"
random_keys[23.4] = "float also works"
random_keys[("ad",12)] = "so tuples"

class AnObject:
    def __init__(self,avalue):
        self.avalue = avalue

my_object = AnObject(12)
random_keys[my_object] = "we can even store objects"
my_object.avalue = 12
try:
    random_keys[[1,2,3]] = "we cant sore lists thought"
except:
    print("unable to store list \n")

for key, value in random_keys.items():
    print("{} has value {}".format(key,value))

