import sys
from notebook import Note, NoteBook

class Menu:

    def __init__(self):
        self.notebook = NoteBook()
        self.choices  = {
            "1":self.show_notes, 
            "2":self.search_notes,
            "3":self.add_note,
            "4":self.modify_note,
            "5":self.quit
        }
    def display_menu(self):
        print("""
            NoteBook Menu:
            1.Show all Notes
            2.Search Notes
            3.Add Note
            4.Modify Notes
            5.Quit
        """)
    
    def run(self):
        "Display Menu Options to the User"
        while True:
            self.display_menu()
            choice = input("Enter Your Option: ")
            action = self.choices.get(choice)
            if action:
                action()
            else:
                print(f"{choice} is not a valid option")

    def show_notes(self, notes=None):
        if not notes:
            notes = self.notebook.notes
        for note in notes:
            print(f"{note.id}: {note.tags}\n{note.memo}")
    
    def search_notes(self):
        filter = input("Search For: ")
        notes = self.notebook.search(filter)
        self.show_notes(notes)
    
    def add_note(self):
        memo = input("Enter the Memo: ")
        self.notebook.new_note(memo)
        print("Note is added successfully")
    
    def modify_note(self):
        id = input("Enter the id of the note: ")
        memo = input("Enter the memo: ")
        tags = input("Enter the tags: ")
        if memo:
            self.notebook.modify_memo(id, memo)
        if tags:
            self.notebook.modify_tags(id, tags)

    def quit(self):
        print("Thank You for using the Notebook app.")
        sys.exit(0)

if __name__ == "__main__":
    Menu().run()