import datetime

last_id = 0


class Note:
    """
    A piece of note in the notebook
    """

    def __init__(self, memo, tags=''):
        self.memo = memo
        self.tags = tags
        self.date_created = datetime.datetime.today()
        global last_id
        last_id += 1
        self.id = last_id

    def match(self, filter):
        """
        Return if the filter element is present or not. --> Boolean Value
        """
        return filter in self.memo or filter in self.tags


class NoteBook:
    """
    Notebook is a collections of notes in the notebook
    """

    def __init__(self):
        self.notes = []

    def new_note(self, memo, tags=''):
        self.notes.append(Note(memo, tags))

    def _find_note(self, note_id):
        for note in self.notes:
            if str(note.id) == str(note_id):
                return note
        return None

    def modify_memo(self, note_id, memo):
        note = self._find_note(note_id)
        if note:
            note.memo = memo
            return True
        print(f"Note with id: {note_id} doesn't exists.")
        return False

    def modify_tags(self, note_id, tags):
        note = self._find_note(note_id)
        if note:
            note.tags = tags
            return True
        print(f"Note with id: {note_id} doesn't exists.")
        return False
        

    def search(self, filter):
        return [note for note in self.notes if note.match(filter)]
