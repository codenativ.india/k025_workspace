import os, zipfile, shutil


class ZipFile:
    def __init__(self, filename, search_string, replace_string):
        self.filename = filename
        self.search_string = search_string
        self.replace_string = replace_string
        self.temp_dir = "unzipped-copy"

    def full_filename(self, filename):
        return os.path.join(self.temp_dir, filename)

    def zip_function(self):
        self.unzip_function()
        self.read_function()
        self.final_function()

    def unzip_function(self):
        os.mkdir(self.temp_dir)
        zip_file = zipfile.ZipFile(self.filename)
        try:
            zip_file.extractall(self.temp_dir)
        finally:
            zip_file.close()

    def read_function(self):
        for filename in os.listdir(self.temp_dir):
            with open(self.full_filename(filename)) as file:
                text = file.read()
            text = text.replace(self.search_string, self.replace_string)
            with open(self.full_filename(filename), 'w') as file:
                file.write(text)

    def final_function(self):
        file = zipfile.ZipFile(f"Copy-{self.filename}", 'w')
        for filename in os.listdir(self.temp_dir):
            file.write(self.full_filename(filename), filename)
        shutil.rmtree(self.temp_dir)


zip_name = input("Enter the Zip filename: ")
search_string = input("Enter the Search term: ")
replace_string = input("Enter the Replace term: ")
c = ZipFile(zip_name, search_string, replace_string)
c.zip_function()
