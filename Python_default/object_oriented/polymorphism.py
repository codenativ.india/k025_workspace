import re

class AudioFile:
    def __init__(self,filename):
        # search_pattern = re.compile(r'\.[a-zA-Z0-9]+$')
        # file_mod = search_pattern.search(filename)
        # c_ext = file_mod.group()
        if not filename.endswith(self.ext):# or c_ext[1:] != self.ext:
            raise Exception("Invalid Extension Error")
        self.filename = filename

class MP3File(AudioFile):
    ext = 'mp3'
    def play(self):
        print(f"Playing the {self.filename} in the extension format of mp3.")
    

class OGGFile(AudioFile):
    ext = 'ogg'
    def play(self):
        print(f"Playing the {self.filename} in the extension format of ogg.")

class WAVFile(AudioFile):
    ext = 'wav'
    def play(self):
        print(f"Playing the {self.filename} in the extension format of wav.")
