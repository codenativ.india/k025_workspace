import authorization

authorization.authenticator.add_user("harris","password")
authorization.authorizor.add_permission("test program")
authorization.authorizor.add_permission("change program")
authorization.authorizor.permit_user("test program","harris")
authorization.authenticator.add_user("camp","uioiuio")
authorization.authorizor.permit_user("change program","camp")

class Menu:
    def __init__(self):
        self.username = None
        self.menu_map = {
            "login":self.login,
            "test":self.test,
            "change":self.change,
            "register":self.register,
            "quit":self.quit
        }

    def login(self):
        logged_in = False
        while not logged_in:
            user = input("Enter Username: ")
            password = input("Enter Password: ")
            try:
                logged_in = authorization.authenticator.login(user,password)
            except authorization.InvalidUserName:
                print("Sorry, Username is not exists.")
            except authorization.InvalidPassword:
                print("Sorry, Password is incorrect.")
            else:
                self.username = user
    
    def register(self):
        username = input("Enter the Username: ")
        password = input("Enter the password: ")
        authorization.authenticator.add_user(username,password)
        print("User has been created successfully")

    def is_permitted(self, permission):
        try:
            authorization.authorizor.check_permission(
                permission, self.username)
        except authorization.NotLoggedInError as e:
            print("{} is not logged in".format(e.username))
        except authorization.NotPermittedError as e:
            print("{} cannot permitted to {}".format(e.username,permission))
        else:
            return True

    def test(self):
        if self.is_permitted("test program"):
            print("Testing the Program now...")
    
    def change(self):
        if self.is_permitted("change program"):
            print("Changing program now...")

    def quit(self):
        raise SystemExit()

    def menu(self):
        try:
            answer = ""
            while True:
                print("""
                Please Enter a command
                \tlogin\tLogin
                \tregister\tRegister
                \ttest\tTest the Program
                \tchange\tChange the Program
                \tquit\tQuit
                """)
                answer = input("enter the command: ").lower()
                try:
                    func = self.menu_map[answer]
                except KeyError:
                    print("Not a valid option.")
                else:
                    func()
        finally:
            print("thank you..!")


Menu().menu()