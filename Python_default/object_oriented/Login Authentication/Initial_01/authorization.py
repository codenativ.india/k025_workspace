import hashlib

class User:
    def __init__(self,username,password):
        self.username = username
        self.password = self._encrypt_password(password)
        self.is_logged_in = False

    def _encrypt_password(self, password):
        hash_string = self.username + password
        hash_string = hash_string.encode("utf8")
        return hashlib.sha256(hash_string).hexdigest()
    
    def check_password(self,password):
        if self.password == self._encrypt_password(password):
            return True
        else:
            return False

class AuthException(Exception):
    def __init__(self,username, user=None):
        super().__init__(username,user)
        self.username = username
        self.user = user

class UserNameExists(AuthException):
    pass

class PasswordTooShort(AuthException):
    pass

class InvalidPassword(AuthException):
    pass

class InvalidUserName(AuthException):
    pass

class NotLoggedIn(AuthException):
    pass

class PermissionError(Exception):
    pass

class NotLoggedInError(AuthException):
    pass

class NotPermittedError(AuthException):
    pass


class Authenticator:
    def __init__(self):
        self.users = {}
    
    def add_user(self,username, password):
        if username in self.users:
            raise UserNameExists(username)
        if len(password) < 6:
            raise PasswordTooShort(username)
        self.users[username] = User(username,password)

    def login(self,username,password):
        try:
            user = self.users[username]
            print(user.username)
        except KeyError:
            raise InvalidUserName(username)
        if not user.check_password(password):
            raise InvalidPassword(username)
        user.is_logged_in = True
        return True

    def user_logged_in(self,username):
        if username in self.users:
            if self.users[username].is_logged_in:
                return True
            else:
                return False
        return "Username is Already Exists."


class Authorizor:
    def __init__(self,authenticator):
        self.authenticator = authenticator
        self.permission = {}
    
    def add_permission(self,perm_name):
        try:
            perm_set = self.permission[perm_name]
        except KeyError:
            self.permission[perm_name] = set()
        else:
            raise PermissionError("Permission Exists.")
    
    def permit_user(self,perm_name, username):
        try:
            perm_set = self.permission[perm_name]
        except KeyError:
            raise PermissionError("Permission does not Exists.")
        else:
            if username not in self.authenticator.users:
                raise InvalidUserName(username)
            perm_set.add(username)

    def check_permission(self,perm_name,username):
        if not self.authenticator.user_logged_in(username):
            raise NotLoggedIn(username)
        try:
            perm_set = self.permission[perm_name]
        except KeyError:
            raise PermissionError("Persmission doesn't exists.")
        else:
            if username not in perm_set:
                raise NotPermittedError(username)
            else:
                return True

authenticator = Authenticator()
authorizor = Authorizor(authenticator)