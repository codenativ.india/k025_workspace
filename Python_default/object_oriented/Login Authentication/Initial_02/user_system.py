import hashlib

class User:
    def __init__(self,username,password):
        self.username = username
        self.password = self._encrypt_password(password)
        self.is_logged = False
    
    def _encrypt_password(self,password):
        hash_string = password + self.username
        hash_string = hash_string.encode("utf8")
        return hashlib.sha256(hash_string).hexdigest()

    def check_password(self,password):
        if self.password == self._encrypt_password(password):
            return True
        else:
            return False

class AuthException(Exception):
    def __init__(self,username,user=None):
        super().__init__(username,user)
        self.username = username
        self.user = user

class PasswordTooShort(AuthException):
    pass

class InvalidUser(AuthException):
    pass

class InvalidPassword(AuthException):
    pass
    
class UserAlreadyExists(AuthException):
    pass

class NotLoggedInError(AuthException):
    pass

class NotPermittedError(AuthException):
    pass

class PermissionAlreadyExists(Exception):
    pass

class PermissionError(Exception):
    pass


class Authenticator:
    def __init__(self):
        self.users = {}

    def add_user(self,username,password):
        if username in self.users:
            raise UserAlreadyExists(username)
        if len(password) < 6:
            raise PasswordTooShort(username)

        self.users[username] = User(username,password)
    
    def login(self,username,password):
        try:
            user = self.users[username]
        except KeyError:
            raise InvalidUser(username)
        if not user.check_password(password):
            raise InvalidPassword(username)
        user.is_logged = True
        return "User has been created Successfully."
    
    def check_status(self,username):
        try:
            user = self.users[username]
        except KeyError:
            raise InvalidUser(username)
        return user.is_logged

class Authorizer:
    def __init__(self,authenticator):
        self.authenticator = authenticator
        self.permission = {}
    
    def add_permission(self,perm_name):
        if perm_name in self.permission:
            raise PermissionError("Already Exists.")
        else:
            self.permission[perm_name] = set()

    def permit_user(self, perm_name, username):
        try:
            perm_set = self.permission[perm_name]
        except KeyError:
            raise PermissionError("Not an valid permission.")
        if not username in self.authenticator.users:
            raise InvalidUser(username)
        perm_set.add(username)

    def check_permission(self, username, perm_name):
        if username in self.authenticator.users:
            if not self.authenticator.check_status(username):
                raise NotLoggedInError(username)
            try:
                perm_set = self.permission[perm_name]
            except:
                raise PermissionError("Not an valid permission.")
            else:
                if username in perm_set:
                    return True
                else:
                    raise PermissionError("User has no valid permission.")
        else:
            return "User is not an registered One."

authenticator = Authenticator()
authorizer = Authorizer(authenticator)