import user_system
from user_system import authenticator, authorizer

authenticator.add_user("harry","secrepassword")
authenticator.add_user("nohara","securepasskey")
authorizer.add_permission("code")
authorizer.add_permission("dance")
authorizer.add_permission("paint")
authorizer.add_permission("view")
authorizer.permit_user(username="harry",perm_name="code")
authorizer.permit_user(username="harry",perm_name="paint")

class Editor:
    def __init__(self):
        self.username = None
        self.key_maps = {
            "login":self.login,
            "register":self.register,
            "permission": self.my_permission,
            "test": self.test,
            "change": self.change,
            "quit": self.quit,
        }

    def login(self):
        username = input("Enter the Username: ")
        password = input("Enter the password: ")
        try:
            authenticator.login(username, password)
        except user_system.InvalidUser:
            print("The User is not an valid one.")
        except user_system.InvalidPassword:
            print("Password was incorrect..")
        else: 
            self.username = username
            print("Logged in...")

    def register(self):
        username = input("Enter the New user Name: ")
        password = input("Enter the password: ")
        try:
            authenticator.add_user(username, password)
        except user_system.PasswordTooShort:
            print("Try something big..")
        except user_system.UserAlreadyExists:
            print("Try Different Username..")
        else: return True

    def my_permission(self):
        try:
            authenticator.check_status(self.username)
        except user_system.InvalidUser:
            print("You are not logged in..") 
        permissions = []
        for permission in authorizer.permission:
            if self.username in authorizer.permission[permission]:
                permissions.append(permission)
        if permissions:
            return [print(i) for i in permissions]
        else:
            print(f"{self.username} has no permissions included.")

    def is_permitted(self, permission):
        try:
            authorizer.check_permission(self.username,permission)
        except user_system.PermissionError:
            print("{} not allowed to {}".format(self.username,permission))
        except user_system.NotLoggedInError:
            print("User is not logged in.")
        else:
            return True
    
    def test(self):
        if self.is_permitted("view"):
            print("Testing the Program...")
            
    def change(self):
        if self.is_permitted("change"):
            print("Changing the Program...")

    def main_role(self):
        while True:
            instruction = ("""
            \tEnter the Option:
            \tlogin --> \tLogin
            \tregister --> \tRegister
            \tpermission --> \tMy permission
            \tquit --> \tQuit
            
            Enter Your Choice: """)
            user_input = input(instruction)
            if user_input in self.key_maps:
                func = self.key_maps[user_input]
                func()
            else:
                print("valid input..")

    def quit(self):
        raise SystemExit()
action = Editor()
action.main_role()
