import requests

url = "https://www.fast2sms.com/dev/bulk"

querystring = {"authorization":"ksTpxr0AEWVHaX6vmz4fQLcNyGCjd5IoPuhbUZ3RJnqe21BgF7LAISxrqftenPk7iYRmp92vdlMzH36W",
"sender_id":"FSTSMS","message":"This is test message form NACFOLG Technologies","language":"english","route":"p","numbers":"9789383625"}

headers = {
    'cache-control': "no-cache"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
