class Silly:
    
    def _get_silly(self):
        print('Getting Silly')
        return self._silly
    
    def _del_silly(self):
        print("Whoa, Killing SILLY")
        del self._silly
    
    def _set_silly(self,value):
        print('Making silly {}'.format(value))
        self._silly = value

    silly = property(_get_silly, _set_silly, _del_silly, "This is a silly property")