import csv, pprint, sys

names = {} # store names with ids
star = {} # store complete details
movies = {} # store movies names with ids

def main():
    
    # store stars stats and names in seprate dictionary
    with open("names.csv",encoding="utf-8") as file:
        reader = csv.DictReader(file)
        for person in reader:
            star[person["id"]] = {
                "name" : person["name"],
                "birth" : person["birth"],
                "movies" : set()
            }
        
            if person["name"].lower() not in names:
                names[person["name"].lower()] = {person["id"]}
            else:
                names[person["name"].lower()].add(person["id"])
    
    # store the movies in a dictionary
    with open("profile.csv",encoding="utf-8") as file:
        reader = csv.DictReader(file)
        for movie in reader:
            movies[movie["movie_id"]] = {
                "movie_name" : movie["name"],
                "cast" : set()
                }
    
    # store the star movies list and update the movies cast list
    with open("stars.csv",encoding="utf-8") as file:
        reader = csv.DictReader(file)
        for person in reader:
            try:
                star[person["person_id"]]["movies"].add(person["movie_id"])
                movies[person["movie_id"]]["cast"].add(person["person_id"])
            except: pass

def result(name1,name2):
    """
    Check for Matching celebrity
    """
    result = 0
    try:
        # return {set} of id, so we slice to get particular id
        name1_id = str(names[name1])[2:-2]
        name2_id = str(names[name2])[2:-2]
        n = set()
        n.add(name1_id)
        n.add(name2_id)
    except KeyError: 
        print("Sorry the person not found")
        query = input("If you want celebrity name press 'l'\n Press any key to exit..!")
        if query == "l":
            for keys in sorted(names.keys()):
                pprint.pprint(keys.title())
        else:
            sys.exit(1)
    for key,movie in movies.items():
        try:
            if (name1_id) in movies[key]['cast']:
                if (name2_id) in movies[key]['cast']:
                    print(f"{name1} and {name2} acted together in the movie {movie['movie_name']}")
                    result += 1
                else: pass
            else: pass
        except KeyError: pass
    if result > 0: return True
    else: print("They didn't acted in same movie yet.")


    

if __name__ == "__main__":
    print("Data Loading..")
    main()
    print("Data Loaded Succesfully..")

    name1 = input("Enter celebrity name: ").lower()
    name2 = input('Enter celebrity to connect with: ').lower()
    result(name1,name2)

# print(movies)
# pprint.pprint(names)
# print("###################################")
# pprint.pprint(star, width=3)
# print('+++++++++++++++++++++++++++++++++++')
# print(type(movies["1001"]['cast']))