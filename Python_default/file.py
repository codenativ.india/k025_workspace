class Solution:
    def numIdenticalPairs(self,nums):
        count = 0
        for index, number in enumerate(nums,start=1):
            for number2 in nums[index:]:
                if number == number2:
                    count += 1
        return count
obj = Solution()
print(obj.numIdenticalPairs([1,2,3,1,1,3]))