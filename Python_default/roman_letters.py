def romanLetters(string):
    roman_values = {"I":1,"V":5,"X":10,"L":50,"C":100,"M":1000}
    length = len(string)
    answer = 0
    ini = 0

    for i in range(length-1,-1,-1):
        if roman_values[string[i]] >= ini:
            answer += roman_values[string[i]]
        else:
            answer -= roman_values[string[i]]
        ini = roman_values[string[i]]
    return answer
        

print(romanLetters("MCIX"))