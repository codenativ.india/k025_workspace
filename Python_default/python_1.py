# list comprehension

names = [
    {'id':0, "name":"adam","city":"brooklyn"},
    {'id':1, "name":"alice","city":"washington"},
    {'id':2, "name":"bob","city":"mexico"},
    {'id':3, "name":"philander","city":"greenwhich"}
]

user_search = [user['name'] for user in names if user['name'][0] == "p"]
print(user_search)

# dict comprehension

new_dict = {user['name']:user['city'] for user in names}
print(new_dict)