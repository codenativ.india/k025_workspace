import requests
res = requests.get(r"https://www.w3.org/TR/PNG/iso_8859-1.txt")
res.raise_for_status()
playFile = open('storylines.txt', 'wb')
for chunk in res.iter_content(1000):
    playFile.write(chunk)
playFile.close()