import turtle

scr = turtle.Screen()
scr.bgcolor("white")
scr.title("MAze game")
scr.setup(700,700)

class Pen(turtle.Turtle):
  def __init__(self):
    turtle.Turtle.__init__(self)
    self.shape("square")
    self.color("black")
    self.penup()
    self.speed(0)

levels = [""]

level_1 = [
    "XXXXXXXXXXXXXXXXXXXXXXXX",
    "XXX   XXXXXXXXXXXXXX   X",
    "XXX   XXXXXXXXXXXXXXXXXX",
    "XXX   XXXXXXXXXXXXXXXXXX",
    "XXX        XXXXX     XXX",
    "XXXXXXXXX  XXXXXXXXX XXX",
    "XXXXXXXXX  XXXXXXXXX XXX",
    "XXXXXXXXX  XXXXXXXXX XXX",
    "XXXXXXXXX    XXXXXXX XXX",
    "XXXXXXXXX    XXXXXXX XXX",
    "XXX    XXXX  XXXXXXX XXX",
    "XXXXXX XXXX      XXX XXX",
    "XXXXXX XXXX  XXX XXX XXX",
    "XXXXXX       XXX     XXX",
    "XXXXXXXXXXX  XXXXXXXXXXX",
    "XXXXXXXXXXX       XXXXXX",
    "XXXXXXXXXXXXXXXXX XXXXXX",
    "XXXXXXXXXXXXXX    XXXXXX",
    "XXXXXXXXXXXX      XXXXXX",
    "XXXXXXXXXXXXXXXXXXXXXXXX",
    "XXXXXXXXXXXXXXXXXXXXXXXX",
    "XXXXXXXXXXXXXXXXXXXXXXXX",
    "XXXXXXXXXXXXXXXXXXXXXXXX",
    "XXXXXXXXXXXXXXXXXXXXXXXX"]

levels.append(level_1)
def set(level):
    for x in range(len(level)):
        for y in range(len(level[x])):
            character = level[x][y]
            screenx = -288 + (y*24)
            screeny = 288 - (x*24)

            if character == "X":
                pen.goto(screenx,screeny)
                pen.stamp()
pen = Pen()

set(levels[1])
turtle.done()
# while True:
# 	pass