def removeVowels(string):
    vowels = ["a","e","i","o","u"]
    result = ""

    for i in string:
        if i in vowels:
            result += ""
        else:
            result += i
    return result

print(removeVowels("base item"))
        