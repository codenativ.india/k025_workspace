def repeated_string(string):
    dic = {'L':0, 'R':0}
    count = 0
    for i in string:
        dic[i] += 1
        if dic['L'] == dic['R']:
            count += 1
            dic['L'] = 0
            dic['R'] = 0
    return count

print(repeated_string("RLRRRLLRLL"))