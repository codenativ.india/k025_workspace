from string  import punctuation

def num_Checker(s):
    return any(i.isdigit() for i in s)

def alphaChecker(s):
    return any(i.isalpha() for i in s)

def punctuation_checker(s):
    return any(punc in s for punc in punctuation)

def passwordValidator(s):
    if len(s) < 7: return False
    word = ""
    name = "password"
    for i in s:
        if 67<= ord(i) <= 90:
            word += i
        elif 97 <= ord(i) <= 122:
            word += i
    if num_Checker(s) and alphaChecker(s) and punctuation_checker(s) and name not in word.lower():
        return True
    else: return False


print(passwordValidator("nameisGood123!@"))