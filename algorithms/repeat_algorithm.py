# def determine(string):
#     letter = {}

#     for i in string:
#         if letter.get(i):
#             letter[i] += 1
#         else:
#             letter[i] = 1
#     for j in letter:
#         if letter[j] == 1:
#             return j
#     return '_'
# print(determine("aabbcc"))
def firstNotRepeatingCharacter(my_string):
  for c in my_string:
    if my_string.index(c) == my_string.rindex(c):
      return c
  return "_"
print(firstNotRepeatingCharacter("aabbcc"))