from collections import Counter
def migrateBirds(arr):
    new = dict(Counter(arr))
    k = max(new.values())
    for key in sorted(new):
        if new[key] == k:
            return key


print(migrateBirds([1,2,3,4,5,4,3,2,1,3,4]))
print(migrateBirds([1,4,4,4,5,3]))
