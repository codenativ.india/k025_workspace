
# Euclid's algorithm 
# two varaibles a, b
# set [ a as 'b' ] and [ b as a%b ] if b is zero return a
def gcd(a,b):
    while (b!=0):
        temp = a
        a = b
        b = temp % a
    return a

print(gcd(144,87))
print(gcd(60,96))