def kangaroo(k1, v1, k2, v2):
    for n in range(10000000000):
        # global n
        if (k1 + v1 == k2 + v2):
            return "YES"
        k1 += v1
        k2 += v2
    return 
user = list(input().split(' '))
user_int = list(map(int, user))
k1 = user_int[0]
v1 = user_int[1]
k2 = user_int[2]
v2 = user_int[3]
print(kangaroo(k1,v1,k2,v2))