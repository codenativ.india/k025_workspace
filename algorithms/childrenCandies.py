def calculateCandies(array, extraCandies):
    max_number = max(array)
    result = []
    for candies in array:
        if candies+extraCandies >= max_number:
            result.append(True)
        else:
            result.append(False)
    return result

print(calculateCandies([12,1,12],10))