def panagram(string):
    alphas = dict()
    for i in range(65,91):
        alphas[chr(i)] = 0
    for char in string.upper():
        if char in alphas.keys():
            alphas[char] += 1
    if all(values >= 1 for key, values in alphas.items()):
        return "panagram"
    else:
        return "not an panagram"
print(panagram("abcdefghijklmnopqrstuvwxy"))