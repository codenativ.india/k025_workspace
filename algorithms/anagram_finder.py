def anagramFinder(s):
    s = s.split(" ")
    count = 0
    s = [i for i in s if len(i) > 1]

    for first in s:
        for second in s[1:]:
            if first == second:
                pass
            elif sorted(first) == sorted(second):
                count += 1
    return count//2

print(anagramFinder("a there are so are are os cars scar"))
