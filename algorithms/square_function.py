import math
def equation(user):
    C = 50
    H = 30
    result = []
    for value in user:
        formula = str(round(math.sqrt((2*C*value)/H)))
        result.append(formula)
    return (','.join(result))

arr = [100,150,180]
print(equation(arr))