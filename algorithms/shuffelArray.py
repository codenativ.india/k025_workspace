class Solution:
    def shuffleArrays(self,array,n):
        array1 = array[0:n]
        array2 = array[n:]
        newList = []

        for i in range(0,len(array1)):
            newList.append((array1[i]))
            newList.append(array2[i])
        return newList

object_ = Solution()
print(object_.shuffleArrays([1,1,2,2],2))

# li= [1,2,3,4,5]
# print(len(li))