class Node():
    def __init__(self, val, L=None, R=None):
        self.val = val
        self.L = None
        self.R = None


class Solution(Node):
    def rangeSumBST(self, root:Node, L, R):
        ans = 0
        stack = [root]
        while stack:
            node = stack.pop()
            if node:
                if L <= node.val <= R:
                    ans += node.val
                if L < node.val:
                    stack.append(node.left)
                if node.val < R:
                    stack.append(node.right)
        print(ans)
        return ans

main = Solution()
ele = main.rangeSumBST([10, 5, 15, 3, 7, None, 18], 7, 15)
