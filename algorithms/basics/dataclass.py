from dataclasses import dataclass

@dataclass
class Solution:
    armor: float
    description: str
    level: int=1

def power(self)-> float:
    return self.armor*self.level

armor = Solution(25, "Hello")
armor.power()

print(armor)