def axis_calculate(arr):
    dict_value = {i[0]:0 for i in arr}
    for i in arr:
        integer = i[2:]
        dict_value[i[0]] += int(integer)
    return dict_value

print(axis_calculate(["x:1","y:2","z:5","z:-2","y:-4"]))