def chocolate(bar,day,month):
    end = month
    result = []
    count = 0
    for start in range(len(bar)):
        lst = bar[start:end]
        end = end+1
        result.append(sum(lst))
    for i in result:
        if i == day:
            count+=1
    return count
        
print(chocolate([4],4,1))

##########################################################

def newChocolate(s,d,m):
    end = m
    result = []
    count = 0
    for start in range(len(s)):
        lst = s[start:end]
        end = end+1
        result.append(sum(lst))
    for i in result:
        if i == d:
            count+=1
    return count
        
print(newChocolate([4],4,1))