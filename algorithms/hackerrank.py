def hackerRankIs(s):
    main = 'hackerrank'
    st = ''
    for i in main:
        for index,j in enumerate(s,1):
            if i == j:
                st += i
                s = s[index:]
                break
    print(st)
    if st == main: return "YES"
    else: return "NO"

# alernate solution simple
def hackerrankInString(s):
    a = 0
    for i in s:
        if a<10 and i == "hackerrank"[a]:
            a+=1
    return "YES" if a==10 else "NO"

print(hackerrankInString("hhhackkerbanker"))


# test cases
print(hackerRankIs("hereiamstackerrank"))
print(hackerRankIs("hhhackkerbanker"))
print(hackerRankIs("knarrekcah"))
print(hackerRankIs("hackerrank"))
print(hackerRankIs("hackeronek"))
print(hackerRankIs("abcdefghijklmnopqrstuvwxyz"))
print(hackerRankIs("rhackerank"))
print(hackerRankIs("ahankercka"))
print(hackerRankIs("hacakaeararanaka"))
print(hackerRankIs("hhhhaaaaackkkkerrrrrrrrank"))
print(hackerRankIs("crackerhackerknar"))











