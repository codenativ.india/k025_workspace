def multiplyFactorial(number):
    if (number <= 1):
        return 1
    else:
        return number*multiplyFactorial(number-1)


print(multiplyFactorial(8))