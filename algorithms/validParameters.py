def validBrackets(s):
    s = s.replace(" ","")
    if len(s) == 0:
        return True
    brackets = ["()","{}","[]"]
    for index,(opening,closing) in enumerate(zip(s,s[1:]),start=1):
        if opening+closing in brackets:
            s = (s[:index-1]+s[index+1:])
            return validBrackets(s)     
        else: pass
    return False

print(validBrackets("["))
print(validBrackets("(]"))
print(validBrackets("([)]"))
print(validBrackets("{ { (      ) } }"))
print(validBrackets("{ { ( { } ) } }"))
print(validBrackets("{ { ( { } ) } "))

