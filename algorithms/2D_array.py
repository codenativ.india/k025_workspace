def Array2d(array):
    array = [int(x) for x in array.split(',')]
    final_array = []
    a = array[0]
    b = array[1]
    for i in range(a):
        single_array = []
        for j in range(b):
            a = i*j
            single_array.append(a)
        final_array.append(single_array)
    return final_array
user = input()
print(Array2d(user))