import string
def ceaser_cipher(k,s):
    lower = list(string.ascii_lowercase)
    values = {}
    result = ""
    for index, letter in enumerate(lower):
        values[letter] = index
    for i in s:
        if i.isalpha():
            if i.isupper():
                i = i.lower()
                index = values[i]
                new_index = (index+k)%26
                new_letter = [key for key,value in values.items() if new_index == value]
                result += new_letter[0].upper()
            else:
                index = values[i]
                new_index = (index+k)%26
                new_letter = [key for key,value in values.items() if new_index == value]
                result += new_letter[0]
        else:
            result += i
    return result

print(ceaser_cipher(2,"abcdefghijklmnopqrstuvwxyz"))