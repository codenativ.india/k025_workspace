
function composite(){
    const my_day = new Date();
    const next_day = new Date('1 January 2021');

    let diffinms = Math.floor(Math.abs(my_day - next_day)/1000);

    const days = Math.floor(diffinms/86400);
    diffinms  -= days * 86400;
    
    const hours = Math.floor(diffinms/3600)%24;
    diffinms -= hours * 3600;

    const minutes = Math.floor(diffinms/60)%60;
    diffinms -= minutes * 60;
    // console.log(parseInt(hours),typeof parseInt(hours),typeof 11);
    if (parseInt(hours)==0 && parseInt(days)==0){
        document.getElementById('days-section').style.display = "none";
        document.getElementById('hours-section').style.display = "none";
    }

    document.getElementById('days').innerHTML = days;
    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('seconds').innerHTML = diffinms;
    console.log("hello");
}

setInterval(composite,1000);
