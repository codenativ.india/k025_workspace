const quizData = [
    {
        question:"Who is the founder of tesla?",
        a:"elon musk",
        b:"nicolas tesla",
        c:"michalle faraday",
        d:"geogria",
        correct:'a'
    },
    {
        question: "Most popular programming language?",
        a: "javascript",
        b: "c",
        c: "Java",
        d: "Python",
        correct: 'a'
    },
    {
        question: "Highgly anticipated os in 2020?",
        a: "ubuntu 20LTS",
        b: "windows 10 2004H",
        c: "mac os Big Sur",
        d: "arch linux",
        correct: 'c'
    },
    {
        question: "when javascript introduced?",
        a: "1994",
        b: "1995",
        c: "1996",
        d: "1997",
        correct:"c"
    }

]

var head_question = document.getElementById('question');
var a_value = document.getElementById('option_a');
var b_value = document.getElementById('option_b');
var c_value = document.getElementById('option_c');
var d_value = document.getElementById('option_d');
var radio_box = document.getElementsByName('answers');
var quizQuestion = 0;
var score = 0;

function getQuestion() {
    question = quizData[quizQuestion];
    head_question.innerText =question.question;
    a_value.innerText =question.a;
    b_value.innerText =question.b;
    c_value.innerText =question.c;
    d_value.innerText =question.d;
    return true;
}
getQuestion();

function unselect() {
    radio_box.forEach(ele => {
        ele.checked = false;
    })
}

function isChecked() {
    for (var i=0; i < radio_box.length; i++){
        if (radio_box[i].checked){
            const selc = (radio_box[i].id);
            return (true,selc);
        }
    }
    return false;
}

var btn = document.getElementById('submit');
btn.addEventListener('click', ()=> {
    var con = isChecked();
    if (con == quizData[quizQuestion].correct) {
        score++;
    }
    if (con[0]) {
        if (quizQuestion > (quizData.length-2)){
        document.querySelector('.quiz_container').style.display = 'none';
        const ele = document.getElementById('result');
        console.log(ele);
        ele.innerHTML = `Your Score is ${score} of ${quizData.length}`
        ele.style.display = 'block';
        return;
    }
    
    // if (quizData[quizQuestion].correct == )
    
    // unselected the next questions options
    unselect();

    // load the next questions and increament by one.
    quizQuestion++;
    getQuestion();
    }else {
        console.log(`select one option for the questions to submit.`)
    }
    
});