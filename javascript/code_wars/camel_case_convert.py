# def to_camel_case(text):
#     #your code here now
#     final_string = ""
#     if len(text) == 0:
#         return ""

#     is_upper = False
#     for i in text:
#         if (i == '-') or (i == '_'):
#             is_upper = True
#             continue
#         if is_upper:
#             result = i.upper()
#         else:
#             result = i
#         is_upper = False
        
#         final_string += result
#     return final_string

def to_camel_case(text):
    #your code here
    final_string = ""
    if len(text) == 0:
        return ""

    is_upper = False
    for i in text:
        if (i == '_') or (i == '-'):
            is_upper = True
            continue
        if is_upper:
            result = i.upper()
        else: result = i
        is_upper = False
        final_string += result
    return final_string


print(to_camel_case('the-stealth-warrior'))
print(to_camel_case('the_stealth_warrior'))
print(to_camel_case('A-B-C'))
print(to_camel_case('the_stealth_warrior'))
