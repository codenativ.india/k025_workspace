def one(arg=None):
    if arg:
        return eval(f"1 {arg}")
    return 1


def two(arg=None):
    if arg:
        return eval(f"2 {arg}")
    return 2


def three(arg=None):
    if arg:
        return eval(f"3 {arg}")
    return 3


def four(arg=None):
    if arg:
        return eval(f"4 {arg}")
    return 4


def five(arg=None):
    if arg:
        return eval(f"5 {arg}")
    return 5


def six(arg=None):
    if arg:
        return eval(f"6 {arg}")
    return 6


def seven(arg=None):
    if arg:
        return eval(f"7 {arg}")
    return 7


def eight(arg=None):
    if arg:
        return eval(f"8 {arg}")
    return 8


def nine(arg=None):
    if arg:
        return eval(f"9 {arg}")
    return 9


def zero(arg=None):
    if arg:
        return eval(f"0 {arg}")
    return 0


def plus(arg):
    return f"+ {arg}"

def minus(arg):
    return f"- {arg}"

def times(arg):
    return f"* {arg}"


def divided_by(arg):
    return f"// {arg}"

print(one(times(one())))
print(five(plus(one(times(one())))))
print(four(plus(nine())))
print(seven(times(five())))
print(eight(minus(three())))
print(six(divided_by(two())))


# def zero(f=None): return 0 if not f else f(0)
# def one(f=None): return 1 if not f else f(1)
# def two(f=None): return 2 if not f else f(2)
# def three(f=None): return 3 if not f else f(3)
# def four(f=None): return 4 if not f else f(4)
# def five(f=None): return 5 if not f else f(5)
# def six(f=None): return 6 if not f else f(6)
# def seven(f=None): return 7 if not f else f(7)
# def eight(f=None): return 8 if not f else f(8)
# def nine(f=None): return 9 if not f else f(9)


# def plus(y): return lambda x: x+y
# def minus(y): return lambda x: x-y
# def times(y): return lambda x: x*y
# def divided_by(y): return lambda x: x//y

# print(five(plus(one(times(one())))))
# print(four(plus(nine())))
# print(seven(times(five())))
# print(eight(minus(three())))
# print(six(divided_by(two())))
