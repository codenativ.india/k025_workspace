
function arrayDiff(a,b){
    final_result = [];
    if ((a.length == 0) && (b.length > 1)){
        return b;
    }else if ((b.length == 0) && (a.length > 1)){
        return a;
    }else{

        // for first array
        for(let arr=0; a.length>arr; arr++){
            if ((b.includes(a[arr]))){
                continue;
            }else{
                final_result.push(a[arr]);
            }
        }

        // for second array
        for(let arr=0; b.length>arr; arr++){
            if (a.includes(a[arr])){
                continue;
            }else{
                final_result.push(b[arr]);
            }
        }
    }

    return final_result;
    
}



console.log(arrayDiff([1],[1,2,3]));
console.log(arrayDiff([], [4,5]));
console.log(arrayDiff([3,4], [3]));
console.log(arrayDiff([3,4], [3]));
console.log(arrayDiff([1,8,2], []));


function array_diff(a, b) {
  return a.filter(e => !b.includes(e));
}