def print_fib(number):
    start = 0
    next = 1
    count = 0

    while True:
        result = start + next
        start = next
        next = result
        if number == 0:
            start = 0
            next = 1
        if (start * next == number):
            return [start, next, True]
        if start * next > number:
            return [start, next, False]


def s_fib(number):
    start, next = 0, 1
    
    while number > start*next:
        start, next = next, start+next
    return [start, next, number == start*next]

# print(print_fib(714))
# print(print_fib(4895))
# print(print_fib(5895))
# print(print_fib(0))


print(s_fib(714))
print(s_fib(4895))
print(s_fib(5895))
print(s_fib(0))