
def conversion_function(number):
    result = ""
    if number == 0:
        return ""
    else:
        result_quo = int(number)/2
        result += str(int(number) % 2)
        return (result)+conversion_function(result_quo)


def decimal_To_binary(number):
    raw_result = conversion_function(number)
    converted_binary = raw_result[::-1][1:]
    return countones(converted_binary)

def countones(digit):
    return sum([int(i) for i in digit])
    
print(str(decimal_To_binary(1234)))
# ori_result = 10011010010
# obt_result = 10011010010
