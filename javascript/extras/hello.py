import os, sys

# settings configrations 
from django.conf import settings
from django.core.wsgi import get_wsgi_application

DEBUG = os.environ.get('DEBUG','on') == 'on'
SECRET_KEY=os.environ.get('SECRET_KEY', os.urandom(32))
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS','localhost').split(',')

settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    ALLOWED_HOSTS = ALLOWED_HOSTS,
    ROOT_URLCONF=__name__,
    MIDDLEWARE_CLASSES=(
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddlware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ),
)
application = get_wsgi_application()
# views configurations
from django.conf.urls import url
from django.http import HttpResponse

def index(request):
    return HttpResponse('Hello world!')

# url configurations
urlpatterns = [
    url(r'^$',index),
]

# manage.py file settings
if __name__ == '__main__':
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
