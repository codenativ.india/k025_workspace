import re


def validate_pin(pin):
    #return true or false
    four_digit = re.search(r"^\d{4}$", pin)
    six_digit = re.search(r"^\d{6}$", pin)
    if four_digit or six_digit:
        return True
    else:
        return False


print(validate_pin('1234'))
