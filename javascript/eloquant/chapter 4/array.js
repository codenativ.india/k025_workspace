
// let userArr = [1,2,3,4,5];

const JOURNAL = require('./journal');

// console.log(userArr.splice(1,2));
// console.log(userArr.length);

// function square(x) {
//     return x*x;
// }
require('./journal.js');

function journalEvents(journal) {
    let events = [];
    for (let entry of journal){
        for (let event of entry.events){
            if (!events.includes(event)){
                events.push(event);
            }
        }
    }
    return events;
}

console.log(journalEvents(JOURNAL));