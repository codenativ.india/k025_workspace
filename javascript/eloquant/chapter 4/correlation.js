const JOURNAL = require('./journal.js');

require('./journal.js');


// finding the co-efficient of correlation
function phi(matrix) {
    return ((matrix[3]*matrix[0] - matrix[1]*matrix[2]) / 
            Math.sqrt(
                (matrix[2]+matrix[3])*
                (matrix[0]+matrix[1])*
                (matrix[1]+matrix[3])*
                (matrix[0]+matrix[2])
            ))
}

// finding the event array of all possibilities
function tableFor(task, journal) {
    let table = [0,0,0,0];
    for(let i=0; i < journal.length; i++) {
        let entry = journal[i], index = 0;
        if (entry.events.includes(task)) index += 1;
        if (entry.squirrel) index += 2;
        table[index] += 1;
    }
    return table;
}

// returning single events
function journalEvents(journal) {
    let events = [];
    for (let entry of journal){
        for (let event of entry.events){
            if (!events.includes(event)){
                events.push(event);
            }
        }
    }
    return events;
}
// console.log(journalEvents(JOURNAL));

function maxValue(arr) {
    var maxValue = [arr[0][0],arr[0][1]];
    for (let i=0; i < arr.length; i++){
        if (arr[i][1] > maxValue[1]) {
            maxValue[1] = arr[i][1];
            maxValue[0] = arr[i][0];
        }
    }
    return maxValue;
}

function result() {
    let mArr = [];
    for (let event of journalEvents(JOURNAL)){
        let correlation = phi(tableFor(event,JOURNAL));
        mArr.push([event, correlation]);
    }
    maxRegi = maxValue(mArr);
    return `${maxRegi[0]} has possibilities of maximum ${maxRegi[1]} of turning you into Squirrel.`;
}

console.log(result());