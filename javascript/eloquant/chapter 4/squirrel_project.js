const JOURNAL = require("./journal.js");

require("./journal.js");

let journal = [];

function addEntry(event, squirrel) {
    journal.push({event, squirrel});
}

addEntry(['work','gym'], false);
addEntry(['brush','gym'], false);
addEntry(['gym','yoga'], true);

console.log(journal);

function phi(matrix) {
    return ((matrix[3]*matrix[0] - matrix[1]*matrix[2]) / 
            Math.sqrt(
                (matrix[2]+matrix[3])*
                (matrix[0]+matrix[1])*
                (matrix[1]+matrix[3])*
                (matrix[0]+matrix[2])
            ))
}

console.log(phi([76, 9, 4, 1]));

// returning the above table
function tableFor(task, journal) {
    let table = [0,0,0,0];
    for(let i=0; i < journal.length; i++) {
        let entry = journal[i], index = 0;
        if (entry.events.includes(task)) index += 1;
        if (entry.squirrel) index += 2;
        table[index] += 1;
    }
    return table;
}

// console.log(tableFor('carrot',JOURNAL));
// console.log(tableFor('pizza',JOURNAL));