
// arrow fuctions ==> 
var power = (base, top) => {
    let result = 1;

    for(let count=0; count<top; count++) {
        result *= base;
    }
    return result;
}

console.log(power(5,2));