// var are represented as global scopes
// let are represented as the block level scope
/*

var newName = 'Django';
var baseName = 'ecmaScript';
var finalVariable = true;

if (finalVariable) {
    let new_line = 'my_line';
    var second_line = 'second_my_line';
    
}

if (newName) console.log("New Name is available");
if (baseName) console.log("Base Name is also available");

console.log(second_line);
console.log(new_line);

*/

const half = function(n) {
    return n/2;
}
let n = 10;
console.log(half(25));