
// nested binding section
const humus = function(factor) {
    const ingredient = function(amount, unit, name) {
        let ingredientAmount = amount * factor;
        if (ingredientAmount > 1){
            unit += 's';
        }
        console.log(`${ingredientAmount} ${unit} ${name}`);
    }
    ingredient(1,'spoon', 'tea');
    ingredient(3,'cup', 'coffee');
};

humus(3);

