function zeropadded(nos, length) {
    string = String(nos)
    for(let i=0; i<length; i++) {
        string = "0" + string
    }
    return string;
}

function farmfunction(chicken, pig, hen) {
    console.log(`${zeropadded(chicken,3)} Chickens`);
    console.log(`${zeropadded(pig,3)} Pigs`);
    console.log(`${zeropadded(hen,4)} Hen`);
}

farmfunction(1,22,25);