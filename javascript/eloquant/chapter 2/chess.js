// chess pattern printing

var line = '';
for(i=1; i<=8; i++) {
    if (i%2==0) {
        for(j=1; j<=8; j++) {
            if(j%2==0){
                line+=" ";
            } else {
                line += "#";
            }
        }
    }else{
        for(j=1; j<=8; j++) {
            if(j%2==0){
                line+="#";
            } else {
                line += " ";
            }
        }
    }
    
    console.log(line);
    line = "";
}