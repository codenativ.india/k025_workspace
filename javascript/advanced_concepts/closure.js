const increasecount = (function() {
    let counter = 0;
    return function () {
        counter++;
        console.log(counter);
    };
})();

// var i=0;
// for(i=0;i<=5; i++){
//     increasecount();
// }

setInterval(increasecount, 1000)